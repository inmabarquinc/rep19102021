import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js'

class PersonaMain extends LitElement {
    
    
  
    static get properties(){
        return{
            //se añade el array con lso datos de entrada
            //lo añadimos como propiedad
            people: {type: Array}

        };
    }

    constructor(){
        super();

        //el array se inicaliza en el constructor
        this.people =[
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley"
                }
            },{
                name: "Bruce Banner",
                yearsInCompany: 15,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner"
                }
            },{
                name: "nombreTres",
                yearsInCompany: 20,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "nombreTres"
                }
            }
        ]

    }

    render (){
        return html`
      
        <h2>Main</h2> 
        <main>
            ${this.people.map(
                person => html`<persona-ficha-listado 
                    fname="${person.name}"
                    yearsInCompany="${person.yearsInCompany}"
                    .photo="${person.photo}"
                ></persona-ficha-listado>`
            )}
            
        </main>   

    `; 
    }
}

customElements.define('persona-main', PersonaMain); 