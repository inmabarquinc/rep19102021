import { LitElement, html } from "lit-element";


class HolaMundo extends LitElement {
    
    //bloque vista que se ve
    render(){
        return html`
            <div>Hola Mundo!</div> 
        `; 
    }
}

customElements.define('hola-mundo', HolaMundo);